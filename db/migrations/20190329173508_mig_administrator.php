<?php

use Phinx\Migration\AbstractMigration;

class MigAdministrator extends AbstractMigration
{
  public function up() {
    $this->execute("alter table user add column is_admin boolean not null default false;");
  }

  public function down() {
    $this->execute("alter table user drop column is_admin;");
  }

}
