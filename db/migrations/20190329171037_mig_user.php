<?php

use Phinx\Migration\AbstractMigration;

class MigUser extends AbstractMigration
{
  public function up() {
    $this->execute("drop table if exists user;");
    $sql = <<<SQL
      create table user (
        username varchar(80) primary key
      , email varchar(80) not null unique
      , password_hash varchar(255) not null unique
      ) ENGINE=INNODB;
SQL;
    $this->execute($sql);
  }

  public function down() {
    $this->execute("drop table user;");
  }
}
