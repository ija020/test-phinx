Dette er en grunnleggende utprøving av Phinx. For å prøve koden,
kopier auth_pdo_example.php til auth_pdo.php og endre
tilkoblingsdetaljene i auth_pdo.php. Start kommandolinjen og finn frem
til denne mappen. Kjør noe tilsvarende

    composer install

... for å hente phinx-pakken, og

    vendor\bin\phinx migrate

(bytt '\' med '/' hvis du kjører på Linux eller Mac)
  Dette skal opprette en tabell 'user' i databasen, som definert av to
migrasjoner i db/migrations. Denne ene oppretter en tabel 'user' med
'username', 'email' og 'password_hash', den andre migrasjonen legger
til en kolonne kalt 'is_admin'.
  Man kan tilbakestille én og én migrasjon med

    vendor\bin\phinx rollback
