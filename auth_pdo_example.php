<?php

define('DB_HOST', 'kark.uit.no');
define('DB_NAME', 'stud_v19_');
define('DB_USER', 'stud_v19_');
define('DB_PASSWORD', 'ditt passord');
define('DB_PORT', 3306);

try {
  $db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,
    DB_USER, DB_PASSWORD);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  die("Database connection failed: " . $e->getMessage());
}

