<?php

require 'auth_pdo.php';

return [
  'paths' => [
    'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
    'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
  ],
  'environments' => [
    'default_database' => 'development',
    'development' => [
      'adapter' => 'mysql',
      'name' => DB_NAME,
      'user' => DB_USER,
      'pass' => DB_PASSWORD,
      'port' => DB_PORT,
      'connection' => $db,
      'charset' => 'utf8'
    ]
  ]
];

